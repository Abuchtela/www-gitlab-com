---
layout: handbook-page-toc
title: "GitLab Data Processing Agreement and Standard Contractual Clauses"
description: "This agreement ..."
---

<a href="https://gitlab.com/gitlab-com/legal-and-compliance/-/blob/17b9b3d3f3b90d1a20ed9b1e5f5ac000e9e8b545/.gitlab/DPA_%2005_23_22.pdf">GitLab Data Processing Addendum</a>

<a href="https://gitlab.com/gitlab-com/legal-and-compliance/-/raw/master/Exhibit_B__Standard_Contractual_Clauses_5_25_22.pdf"> Exhibit B - Standard Contractual Clauses</a>
