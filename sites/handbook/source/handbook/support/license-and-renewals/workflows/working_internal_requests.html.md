---
layout: handbook-page-toc
title: Handling L&R Internal Requests
category: Working L&R requests
description: Describes how to service internal requests for licensing & renewals.
---

- TOC
{:toc .hidden-md .hidden-lg}

----

### Servicing Internal Requests

Choose the best [L&R Workflows](/handbook/support/license-and-renewals/workflows/) when servicing L&R internal requests.

**NOTE:** L&R Internal Requests can be found in the "L&R" View in Zendesk. 

